<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Presentation du jeu</title>
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../CSS/code.css"> 
    <style type="text/css"></style>
  </head>
  
 
<body>
<?php include '../include/header_public.php';

// on ne peut arriver sur cette page si il n'y pas de jeu (il faut passer par rechercher_test.php auparavant)
   if ($_SESSION['id_jeu']!="" || isset($_POST['jeu']) ) {
	include '../include/connexionbdd.php';
	if ($_SESSION['id_jeu']=="") { // si l'id du jeu n'est pas déjà enregistrer il faut le faire afin de ne pas perdre le jeu (pour retour donner_avis par ex)
		$_SESSION['id_jeu']=$_POST['jeu'];
	}
	
	$requete = "SELECT * from ppe2.jeux_video.jeu where id_jeu =".$_SESSION['id_jeu'].";";
        $resultats=$connexion->query($requete);
        $ligne = $resultats->fetch();

?> 
   
    <h1>Presentation du jeu</h1>
  
 <form method="POST">
<!--Nom du jeu-->
    <h2 class="titre">
      <?php echo $ligne['nom']; ?>
    </h2> <br><a href="recherche_test.php" style="margin-left:85%;">Précédent</a><hr><br>
<div  style="height:100%; display:flex;">
  <div class="gauche"><!--Informations du côté gauche de l'écran-->
    <!--Image du jeu-->
    <img class="imgjeu" src="../images/jeux/<?php echo $ligne['image'] ?>"/><br><br>

    
    <!--Informations générales du jeu-->
    <div class="carreinfo">

   <!--Createur-->
    <div class="infogenerale">Createur :<br>
    <?php $requete = "SELECT createur.nom from jeux_video.createur
                      inner join jeux_video.concevoir on createur = id
                      inner join jeux_video.jeu on jeu = id_jeu
                      where id_jeu =".$_SESSION['id_jeu'].";";
    $resultats=$connexion->query($requete);
    $ligne = $resultats->fetch();
    echo $ligne['nom']; ?></div>

  <!--Date de création-->
    <br><div class ="infogenerale">Date de création :<br>
    <?php $requete = "SELECT date_creation from jeux_video.jeu
                      where id_jeu =".$_SESSION['id_jeu'].";";
    $resultats=$connexion->query($requete);
    $ligne = $resultats->fetch();
    echo $ligne['date_creation']; ?>
  </div>

  <!--Prix-->
  <br><div class="infogenerale"> Prix :<br>
      <?php $requete = "SELECT prix from jeux_video.jeu
                        where id_jeu =".$_SESSION['id_jeu'].";";
              $resultats=$connexion->query($requete);
              $ligne = $resultats->fetch();
              echo $ligne['prix'];?>€</div>

   <!--Categorie du jeu-->
             <br> <div class="infogenerale">Catégorie du jeu :<br>
                <?php $requete = "SELECT nom_categ FROM jeux_video.jeu
                                  inner join jeux_video.categ_jeu on id_j = id_jeu
                                  inner join jeux_video.categorie on id = id_c
                                  where id_jeu =".$_SESSION['id_jeu'].";";
                        $resultats=$connexion->query($requete);
                        $ligne = $resultats->fetch();
                        echo $ligne['nom_categ'];?><br></div>

   <!--Plateforme-->
   <br> <div class="infogenerale">Plateforme :<br>
      <?php $requete = "SELECT libelle from jeux_video.plateforme
                        inner join jeux_video.platef_jeu on platef = id_plateforme
                        inner join jeux_video.jeu on jeu = id_jeu
                        where id_jeu =".$_SESSION['id_jeu'].";";
              $resultats=$connexion->query($requete);
              while ($ligne = $resultats->fetch()) {
                echo $ligne['libelle'];?><br>
                <?php
                }
                ?>

            </div>

  <!--Configuration-->
  <br>  <div class="infogenerale"><u>Configuration minimale :</u><br><br>
      <?php $requete = "SELECT cpu , se , ram, carte_graphique, hdd from jeux_video.jeu where id_jeu =".$_SESSION['id_jeu'].";";
              $resultats=$connexion->query($requete);
              $ligne = $resultats->fetch();?>
              CPU : <br><?php echo $ligne['cpu'];?><br><br>
              Systeme Exploitation : <br><?php echo $ligne['se'];?><br><br>
              RAM : <br><?php echo $ligne['ram'];?><br><br>
              Carte graphique : <br><?php echo $ligne['carte_graphique'];?><br><br>
              Espace mémoire : <br><?php echo $ligne['hdd']; ?>
            </div><br>
    </div>
  </div>  <!-- fin div gauche  -->

 

      <!--Informations au centre-->
    <div class="milieux">
     <h2>Note : <?php $requete = "SELECT ROUND ( avg (note)) FROM jeux_video.donner_avis
                                                    inner join jeux_video.jeu on donner_avis.jeu = jeu.id_jeu
                                                    WHERE id_jeu =".$_SESSION['id_jeu'].";";
                     $resultats=$connexion->query($requete);
                     $ligne = $resultats->fetch();  
		     if ($ligne['round']!="") {
	                     echo ' '.$ligne['round']; 
		     } else {
			     echo '<span style="font-size:16px;"> pas de note</span>';
		     }	?>
     </h2>
     <!--<div class="droit">-->

        <!--Bouton favoris-->
        <h4 class="favoris">
          <input type="checkbox" id="1" name="1" value="favoris">
          <label for="1"> Ajouter à vos favoris</label>
        </h4>
	
    <!-- </div> --> <!-- fin div droit  -->

      <!--Description-->
      <br><h2 class="description">Description du jeu</h2>
      <div class="cadre"><?php $requete = "SELECT description from jeux_video.jeu where id_jeu =".$_SESSION['id_jeu'].";";
      $resultats=$connexion->query($requete);
      $ligne = $resultats->fetch();
      echo $ligne['description']; ?></div>

      <!--DLC-->
     <br> <h2 class="dlc">DLC</h2>
      <div class="cadre"><?php $requete ="SELECT jeux_video.dlc.nom from jeux_video.dlc
                                           inner join jeux_video.jeu on jeu.id_jeu = dlc.jeu
                                           where id_jeu =".$_SESSION['id_jeu'].";";
      $resultats=$connexion->query($requete);

      if ( $resultats->fetch() == null ) {
        echo "Ce jeu ne possède pas de dlc.";
      }

      while ($ligne = $resultats->fetch()) {
        echo $ligne['nom'];?><br>
        <?php
        }
      ?>
      </div>

      <!--Competitions-->
      <br><h2 class="Compet">Type de competition</h2>
      <div class="cadre"><?php $requete = "SELECT type_compet.libelle from jeux_video.type_compet
                                           inner join jeux_video.compet_etre_type on type_c = id
                                           inner join jeux_video.competition on competition.id = type_c
                                           inner join jeux_video.jeu on competition.id_jeu = jeu.id_jeu
                                           where jeu.id_jeu =".$_SESSION['id_jeu'].";";
      $resultats=$connexion->query($requete);

    $i=0;
    while ($ligne = $resultats->fetch()) {
      echo $ligne['libelle'];
      $i++;
    }
    if ($i==0)  {
      echo "jeu sans compet";
     }

    ?><br></div>

      <!--Espace commentaire-->
      <br><h2 class="comment">Espace commentaire</h2> 
      <div class="cadre">
      <?php $requete = "SELECT pseudo ,avis, note FROM jeux_video.donner_avis
                         inner join jeux_video.utilisateur  on id_utilisateur = aviseur
						             inner join jeux_video.jeu on jeu =  id_jeu
                         where jeu.id_jeu =".$_SESSION['id_jeu'].";";
      $resultats=$connexion->query($requete);
    while ($ligne = $resultats->fetch()) {
      echo $ligne['pseudo'];?> :  "
      <?php      echo $ligne['avis'];?> "<i> Note :
      <?php      echo $ligne['note'].'</i>';?><br>
   <?php
   }
    ?></div> </form><!--div cadre --> 
<?php if (isset($_SESSION['pseudo']) && $_SESSION['pseudo'] != null && $_SESSION['id_user']!=null) {
  ?>
      <h4 style="text-align:left"> Laisser un commentaire </h4>
            <form action="donner_avis.php" method="post" style="text-align:left;">
		<input type="hidden" name="jeux" value="<?php echo $_SESSION['id_jeu']; ?>"/>
		<input type="submit" class="btn btn-success" style="text-align:right"value="Commenter"/>
            </form>
          <?php } // fin si connexion?>
        </div>  <!-- fin div milieu  -->

  <!--Informations du coté droit-->
  
     </div>
   
<?php  include "../include/footer_public.php"; } // fin si "il faut arriver par recherche_test.php"
	else { ?> <h4>pas de jeu sélectionné, retourner à la page de recherche ===> <a href="recherche_test.php"> retour</a> </h4> <?php }  ?>
  </body>
</html>
