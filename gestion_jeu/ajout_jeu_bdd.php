<?php
session_start();
include '../include/connexionbdd.php';

// est-ce qu'il y a une image de jeu ?
if (isset($_FILES['photo']) && $_FILES['photo']['error']==0) {
	// Testons si le fichier n'est pas trop gros (moins de 1Mo ici)
       	if ($_FILES['photo']['size'] <= 1000000)
       	{
	    // Testons si l'extension est autorisée
		$infosfichier = pathinfo($_FILES['photo']['name']);
		$extension_upload = $infosfichier['extension'];
		$extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
		if (in_array($extension_upload, $extensions_autorisees))
		{
		    // On peut valider le fichier et le stocker définitivement
		    move_uploaded_file($_FILES['photo']['tmp_name'], '../images/jeux/'.basename($_POST['nom_jeu'].".".$infosfichier['extension']));
		    // tentative d'ajout du jeu  pour stockage sur postgresql 
		    $req_ajout = "insert into jeux_video.jeu (description, date_creation, image, prix, cpu, se, ram, carte_graphique, hdd, nom)
	VALUES ('".$_POST['description']."','".$_POST['date']."','".($_POST['nom_jeu'].".".$infosfichier['extension'])."',".$_POST['prix'].", '".$_POST['cpu']."', '".$_POST['se']."', '".$_POST['ram']."', '".$_POST['matos']."', '".$_POST['hdd']."', '".$_POST['nom_jeu']."');";
		    $ajout_j = $connexion->exec($req_ajout);

	  	    // il faut récupérer l'id du jeu que l'on vient d'insérer (le dernier donc avec id max
		     $req_trouve_id = "SELECT max(id_jeu) as max from jeux_video.jeu;";
		     $id_jeu = $connexion->query($req_trouve_id)->fetch();  // id du jeu qui vient d'être inséré

                    // ajout des créateurs du jeu
                    $tab=$_POST["createur"];  // on récupère les sélections dans un tableau
                     foreach($tab as $c){  // pour chaque élément du tableau, on créé une nouvelle ligne dans la table concevoir 
                  	   $req_ajout_c = "INSERT INTO jeux_video.concevoir (jeu, createur) VALUES (".$id_jeu['max'].", ".$c.");";
		   	   $ajout_c = $connexion->exec($req_ajout_c);
                	 }
                	  
              
		    // ajout des plateformes du jeu
		     $tab=$_POST["plateforme"];  // on récupère les sélections dans un tableau
                     foreach($tab as $p){  // pour chaque élément du tableau, on créé une nouvelle ligne dans la table platef_jeu 
                  	   $req_ajout_p = "INSERT INTO jeux_video.platef_jeu (jeu, platef) VALUES (".$id_jeu['max'].", ".$p.");";
			   $ajout_p = $connexion->exec($req_ajout_p);
                	 }
		    // retour à la page ajout_jeu.php
		    header("Location: ajout_jeu.php?message=le jeu ".$_POST['nom_jeu']." a bien été ajouté !! ");
					
		} else {  // extension non ok
		  	   header("Location: ajout_jeu.php?message=type d'image non accepté (choisir jpg, jpeg,gif ou png)!");
		       } 			
         } else { // fichier image trop lourd
	  	  header("Location: ajout_jeu.php?message=fichier image trop volumineux !!");
                }
} else { // fichier non existant
   header("Location: ajout_jeu.php?message=il faut télécharger un fichier image du jeu !!");
   }

?>
