$(function(){
   /*********************************** Page connexion************************************/

    $('#lien_connexion').click( function(e) {   // clic sur bouton connexion dans la barre de navig
			var recup = $("#lien_connexion").html();  // on récupère la valeur actuelle du bouton : connexion ou déconnexion
			$.post("../ajax/gererconnexion.php",{
                              // valorise les deux arguments passés à la fonction traiterconnexion
                                "valeur" : recup},
                                foncRetourCoDeconnexion );
     });

     function foncRetourCoDeconnexion(data){
		if (data=="deconnexion_ok")
		{

                	$("#lien_connexion").html("Connexion");
		        var styles = {
                             color : "red",
                             "text-align": "center"
                            };
	                $("#tralala").css( styles );
			$("#tralala").html("déconnexion ok");
			alert("Vous êtes déconnecté");

			$("#connexion").modal("hide");
		}

     }

    $('#btnconnexion').click( function(e) {
                        // les deux lignes annulent le comportement par défaut du clic
                        // c'est à dire href="#" qui provoquerait un rappel de la même page
                          e.preventDefault();
                          e.stopPropagation();

                          if ($("#password").val()!="" && $("#login").val()!="") {

                          var mdp = $("#password").val(); //récupère le contenue de la zone d'id mdp
                          var login = $("#login").val();

                          /* appel d'une fonction ajax */
                          // elle prend 3 arguments :
                          // 1- le nom de la fonction php qui sera exécutée
                          // 2- la liste des arguments à fournir à cette fonction
                          // 3- le nom de la fonction JS qui sera exécutée au "retour" du serveur
                          $.post("../ajax/traiterconnexion.php",{
                              // valorise les deux arguments passés à la fonction traiterconnexion
                                "mdp" : mdp,
                                "login" : login},
                                foncRetourConnexion );
                              } // fin si
                              else {
                                var styles = {
                                                        color : "red",
                                                        "text-align": "center"
                                                         };
                                $("#message").css( styles );
                                $("#message").html("Attention - remplir tous les champs !");
                              }
       });

     /* fonction JS qui sera exécutée après le retour de l'appel ajax précedent */
     // le paramètre data représente la donnée envoyée par le serveur
     function foncRetourConnexion(data){
            if(data.length != 0){
               var styles = {
                             color : "red",
                             "text-align": "center"
                             };
                $("#tralala").css( styles );
                $("#tralala").html("connexion ok");

	        $("#connexion").modal("hide");
                $("#lien_connexion").html("Déconnexion");
		//$("#lien_connexion").val("Déconnexion");
                // appel à une autre page ou un autre menu ou autre chose
             }
             else{
             // sinon affichage d'un message en rouge dans la div d'id message
                var styles = {
                             color : "red",
                             "text-align": "center"
                             };
                $("#message").css( styles );
                $("#message").html("Erreur de login et/ou mdp");
             }
     }

     $('#btninscription').click( function(e) {
                           e.preventDefault();
                           e.stopPropagation();
			if ($("#nom").val()!="" && $("#prenom").val()!="" && $("#email").val()!="" && $("#pseudo").val()!=""
				&& $("#passwordi").val()!= "" && $("#passwordii").val() !="" && $("#datenaiss").val()!=""
				&& $("#nationalite").val() != "" && $("#verif").val()!="") {
				           var nom = $("#nom").val();
				           var prenom = $("#prenom").val();
				           var email = $("#email").val();
				           var pseudo = $("#pseudo").val();
				           var mdp1 = $("#passwordi").val();
				           var mdp2 = $("#passwordii").val();
				           var date_naiss = $("#datenaiss").val();
				           var nationalite = $("#nationalite").val();
				           var question_verif = $("#verif").val();

				           $.post("../ajax/traiterinscription.php",{
				                 "nom" : nom,
				                 "prenom" : prenom,
				                 "email" : email,
				                 "pseudo" : pseudo,
				                 "mdp1" : mdp1,
				                 "mdp2" : mdp2,
				                 "date_naiss" : date_naiss,
				                 "nationalite" : nationalite,
				                 "question_verif" : question_verif},
				                 foncRetourInscription );
			} // fin si
			else {
				var styles = {
                                color : "red",
                                "text-align": "center"
                                 };
				$("#message2").css( styles );
				$("#message2").html("Attention - remplir tous les champs !");
			}
        });

        function foncRetourInscription(data){
                var styles = {
                                color : "red",
                                "text-align": "center"
                             };

		if(data == "ok") {
                   $("#tralala").css( styles );
                   $("#tralala").html("inscription ok - veuillez vous connecter");

   	           $("#inscription").modal("hide");
                   //$("#lien_connexion").html("Déconnexion");
                }
                else {
		  $("#message2").css( styles );
		  if (data=="pb_mdp") {

                   	$("#message2").html("Erreur d'inscription -- les mots de passe saisis sont différents");
		  }
		  else if (data=="pb_email") {
			$("#message2").html("Erreur d'inscription -- l'email renseigné est déjà existant");
		  }
      else if (data=="pb_pseudo"){
      $("#message2").html("Erreur d'inscription -- le pseudo renseigné est déjà existant");
      }
                }
        }


}); // fin fonction principale/*
